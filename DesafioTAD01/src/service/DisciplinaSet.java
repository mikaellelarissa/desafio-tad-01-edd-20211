package service;

import entitie.Disciplina;

import java.util.List;

public interface DisciplinaSet {
	
	public boolean contains(List <Disciplina> disciplina, Disciplina disciplina1);
	
	public boolean add(List <Disciplina> disciplina, Disciplina disciplina1);
	
	public boolean remove(List <Disciplina> disciplina, Disciplina disciplina1);
	
	public int size(List <Disciplina> disciplina);
	
	public void clear(List <Disciplina> disciplina);
	
}
