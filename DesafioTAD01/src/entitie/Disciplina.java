package entitie;

import service.DisciplinaSet;

import java.util.List;

public class Disciplina implements DisciplinaSet {

	private String nome;
	private int semestre;

	public Disciplina() {

	}

	public Disciplina(String nome, int semestre) {
		this.nome = nome;
		this.semestre = semestre;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public int getSemestre() {
		return semestre;
	}

	public void setSemestre(int semestre) {
		this.semestre = semestre;
	}

	@Override
	public boolean contains(List<Disciplina> disciplina, Disciplina disciplina1) {
		boolean consultar;
		consultar = disciplina.contains(disciplina1);
		return consultar;
	}

	@Override
	public boolean add(List<Disciplina> disciplina, Disciplina disciplina1) {
		boolean adicionar;
		adicionar = disciplina.add(disciplina1);
		return adicionar;
	}

	@Override
	public boolean remove(List<Disciplina> disciplina, Disciplina disciplina1) {
		boolean remover;
		remover = disciplina.remove(disciplina1);
		return remover;
	}

	@Override
	public int size(List<Disciplina> disciplina) {
		return disciplina.size();
	}

	@Override
	public void clear(List<Disciplina> disciplina) {
		disciplina.clear();
	}

}
