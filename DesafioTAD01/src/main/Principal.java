package main;

import entitie.Disciplina;

import java.util.List;
import java.util.ArrayList;
import java.util.Scanner;

public class Principal {
	
	static Scanner sc = new Scanner(System.in);
	static List<Disciplina> disciplina = new ArrayList<>();

	public static void main(String[] args) {
		
		String nome = " ";
		int semestre = 0;
		Disciplina d = new Disciplina();

		char escolha = 's';
		while (escolha == 's') {
			System.out.println("Informe o nome da disciplina: ");
			nome = sc.nextLine();
			System.out.println("Informe o semestre da disciplina: ");
			semestre = sc.nextInt();
			
			d = new Disciplina();

			d.add(disciplina, d);
			System.out.println("Deseja cadastrar uma outra disciplina?");
			sc.nextLine();
			escolha = sc.nextLine().charAt(0);
		}

		String teste = " ";
		if (d.contains(disciplina, d)) {
			teste = "existe";
		} else {
			teste = "n�o existe";
		}

		d.remove(disciplina, d);
		d.add(disciplina, d);

		System.out.println("A disciplina " + teste + "\nA quantidade de disciplinas � " + d.size(disciplina));
		
		String r = " ";
		System.out.println("Deseja limpar as disciplinas? s - sim   n - n�o");
		r = sc.nextLine();
		if(r.equals("s")) {
			d.clear(disciplina);
			System.out.println("A quantidade de disciplinas agora � " + d.size(disciplina));
		} else {
			System.out.println("A quantidade de disciplinas � " + d.size(disciplina));
		}
	}
}
